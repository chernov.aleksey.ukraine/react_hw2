import React from "react";
import "./App.scss";
import ItemList from "./Components/ItemList/ItemList";
import Header from "./Components/Header/Header";
import ModalWindow from "./Components/Modalwindow/Modalwindow";

let counter = 0;

class App extends React.PureComponent {
  state = {
    array: [],
    isModalOpen: false,
    chosenItem: {},
  };
  addToCart = (art) => {
    this.setState((current) => {
      const array = [...current.array];
      const index = array.findIndex((el) => el.art === art);

      if (index !== -1) {
        array[index].isCart = true;
      }
      localStorage.setItem("array", JSON.stringify(array));
      return { array };
    });
  };
  handleClick = (value) => {
    this.setState({ isModalOpen: value });
  };
  chooseItem = (value) => {
    this.setState({ chosenItem: value });
  };
  addToFavor = (card) => {
    this.setState((current) => {
      const array = [...current.array];
      const index = array.findIndex((el) => el.art === card.art);
      if (index !== -1) {
        array[index].isFavorite = true;
      }
      localStorage.setItem("array", JSON.stringify(array));
      return { array };
    });
  };
  moveFromCart = (card) => {
    this.setState((current) => {
      const array = [...current.array];
      const index = array.findIndex((el) => el.art === card.art);
      if (index !== -1) {
        array[index].isCart = false;
      }
      localStorage.setItem("array", JSON.stringify(array));
      return { array };
    });
  };
  moveFromFavor = (card) => {
    this.setState((current) => {
      const array = [...current.array];
      const index = array.findIndex((el) => el.art === card.art);
      if (index !== -1) {
        array[index].isFavorite = false;
      }
      localStorage.setItem("array", JSON.stringify(array));
      return { array };
    });
  };
  favorCount = ({ array }) => {
    counter = 0;
    array.forEach((el) => {
      if (el.isFavorite) {
        counter += 1;
      }
    });
    return counter;
  };
  cartCount = ({ array }) => {
    counter = 0;
    array.forEach((el) => {
      if (el.isCart) {
        counter += 1;
      }
    });
    return counter;
  };
  async componentDidMount() {
    if (localStorage.getItem("array")) {
      this.setState({ array: JSON.parse(localStorage.getItem("array")) });
    } else {
      this.setState({
        array: await fetch(`./items.json`).then((res) => res.json()),
      });
    }
    
  }

  render() {
    const { array, isModalOpen, chosenItem } = this.state;
    console.log(this.state);
    return (
      <div className="App">
        {!isModalOpen && (
          <Header
            array={array}
            favorCount={this.favorCount}
            cartCount={this.cartCount}
          />
        )}
        <ItemList
          array={array}
          addToFavor={this.addToFavor}
          moveFromCart={this.moveFromCart}
          moveFromFavor={this.moveFromFavor}
          chooseItem={this.chooseItem}
          handleClick={this.handleClick}
        />
        <ModalWindow
          isModalOpen={isModalOpen}
          addToCart={this.addToCart}
          handleClick={this.handleClick}
          chosenItem={chosenItem}
          headertext={"Adding item to the cart."}
          maintext1={"Do you want to add"}
          maintext2={"to the cart?"}
        />
      </div>
    );
  }
}
export default App;
