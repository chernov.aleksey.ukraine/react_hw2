import React from "react";
import "./Button.scss";
import PropTypes from "prop-types";

class Button extends React.PureComponent {
  render() {
    const { onClick, text } = this.props;
    return (
      <div>
        <button onClick={onClick}>{text}</button>
      </div>
    );
  }
}
Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  text: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node])
    .isRequired,
};

export default Button;
