import React from "react";
import Card from "../Card/Card";
import "./ItemList.scss";
import PropTypes from "prop-types";

class ItemList extends React.PureComponent {
  render() {
    const {
      array,
      addToFavor,
      moveFromCart,
      moveFromFavor,
      handleClick,
      chooseItem,
    } = this.props;
    return (
      <div className="itemcontainer">
        {array.map(({ name, art, color, price, url, isFavorite, isCart }) => (
          <Card
            key={art}
            name={name}
            art={art}
            color={color}
            price={price}
            url={url}
            isFavorite={isFavorite}
            isCart={isCart}
            addToFavor={addToFavor}
            moveFromCart={moveFromCart}
            moveFromFavor={moveFromFavor}
            handleClick={handleClick}
            chooseItem={chooseItem}
          />
        ))}
      </div>
    );
  }
}
ItemList.propTypes = {
  moveFromFavor: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  moveFromCart: PropTypes.func.isRequired,
  addToFavor: PropTypes.func.isRequired,
  chooseItem: PropTypes.func.isRequired,
  array: PropTypes.array.isRequired,
};
export default ItemList;
