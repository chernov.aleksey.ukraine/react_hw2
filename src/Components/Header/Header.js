import React from "react";
import "./Header.scss";
import PropTypes from "prop-types";

class Header extends React.PureComponent {
  render() {
    const { array, favorCount, cartCount } = this.props;
    return (
      <div className="headercountcontainer">
        <p>&#9733;</p>
        <div>{favorCount({ array })}</div>
        <div>
          <img className="headercartlogo" src="./cart.png" alt="" />
        </div>
        <div>{cartCount({ array })}</div>
      </div>
    );
  }
}
Header.propTypes = {
  favorCount: PropTypes.func.isRequired,
  cartCount: PropTypes.func.isRequired,
  array: PropTypes.array.isRequired,
};

export default Header;
